from django.apps import AppConfig


class AnimedbConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'animedb'
