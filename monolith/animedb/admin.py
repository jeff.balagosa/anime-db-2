from django.contrib import admin
from .models import Genre, Series, Studio
# Register your models here.

@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ("name", "description")

@admin.register(Series)
class SeriesAdmin(admin.ModelAdmin):
    pass

@admin.register(Studio)
class StudioAdmin(admin.ModelAdmin):
    pass
