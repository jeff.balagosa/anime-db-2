from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Genre, Series, Studio
from common.json import ModelEncoder


# Series Encoders
class SeriesListEncoder(ModelEncoder):
    model = Series
    properties = ["name",  "description", "num_seasons", "num_episodes", "genre", "studio"]


class SeriesDetailEncoder(ModelEncoder):
    model = Series
    properties = ["name", "description", "num_seasons", "num_episodes", "genre", "studio"]


# Studio Encoders
class StudioListEncoder(ModelEncoder):
    model = Studio
    properties = ["name"]


class StudioDetailEncoder(ModelEncoder):
    model = Studio
    properties = ["name", "description"]


@require_http_methods(["GET"])
def api_list_genres(request):
    genres = Genre.objects.order_by("name")
    genre_list = []
    for genre in genres:
        values = {
            "name": genre.name,
            "description": genre.description,
        }
        genre_list.append(values)
    return JsonResponse({"genres": genre_list})


@require_http_methods(["GET", "POST"])
def api_list_series(request):
    if request.method == "GET":
        series = Series.objects.all()
        return JsonResponse(
            {"series": series},
            encoder=SeriesListEncoder,
        )
    else:
        content = json.loads(request.body)

        series = Series.objects.create(**content)
        return JsonResponse(
            series,
            encoder=SeriesDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_series(request, pk):
    if request.method == "GET":
        series = Series.objects.get(id=pk)
        return JsonResponse(
            {"series": series},
            encoder=SeriesDetailEncoder,
        )
    elif request.method == "DELETE":
        try:
            series = Series.objects.get(id=pk)
            series.delete()
            return JsonResponse(
                series,
                encoder=SeriesDetailEncoder,
                safe=False,
            )
        except Series.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_studios(request):
    if request.method == "GET":
        studios = Studio.objects.all()
        return JsonResponse(
            {"studios": studios},
            encoder=StudioListEncoder,
        )
    else:
        content = json.loads(request.body)

        studio = Studio.objects.create(**content)
        return JsonResponse(
            studio,
            encoder=StudioDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_studio(request, pk):
    if request.method == "GET":
        studio = Studio.objects.get(id=pk)
        return JsonResponse(
            {"studio": studio},
            encoder=StudioDetailEncoder,
        )
    elif request.method == "DELETE":
        try:
            studio = Studio.objects.get(id=pk)
            studio.delete()
            return JsonResponse(
                studio,
                encoder=StudioDetailEncoder,
                safe=False,
            )
        except Studio.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
