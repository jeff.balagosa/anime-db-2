from django.urls import path

from .api_views import (
    api_list_genres,
    api_list_series,
    api_show_series,
    api_list_studios,
    api_show_studio
)


urlpatterns = [
    path("genres/", api_list_genres, name="api_list_genres"),


    path("series/", api_list_series, name="api_list_series"),
    path("series/<int:pk>/", api_show_series, name="api_show_series"),

    path("studios/", api_list_studios, name="api_list_studios"),
    path("studios/<int:pk>/", api_show_studio, name="api_show_studio"),
]
