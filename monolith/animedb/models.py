from django.db import models
from django.urls import reverse


class Genre(models.Model):
    """
    The Genre model represents an anime genre.

    Genre is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    name = models.CharField(max_length=40, unique=True)
    description = models.TextField(null=True, blank=True, max_length=500)


class Studio(models.Model):
    """
    The Studio model describes an Anime studio.
    """

    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True, max_length=500)
    updated = models.DateTimeField(auto_now=True)

    def get_api_url(self):
        return reverse("api_show_studio", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)  # Default ordering for Studio


class Series(models.Model):
    """
    The Series model describes an Anime series.
    """

    name = models.CharField(max_length=200)
    description = models.TextField()
    num_seasons = models.PositiveSmallIntegerField()
    num_episodes = models.PositiveSmallIntegerField()
    updated = models.DateTimeField(auto_now=True)

    genre = models.CharField(max_length=200)
    studio = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_show_series", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)  # Default ordering for Series
