import React, { useState, useEffect } from "react";

function SeriesForm() {
  // Call useState to create a variable and a function to update that variable
  const [name, setName] = useState([]);
  const [seasons, setSeasons] = useState([]);
  const [episodes, setEpisodes] = useState([]);
  const [genre, setGenre] = useState([]);
  const [description, setDescription] = useState([]);
  const [studio, setStudio] = useState([]);

  // Create a callback function that updates name variable when input changes (i.e. user types)
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleSeasonsChange = (event) => {
    const value = event.target.value;
    setSeasons(value);
  };

  const handleEpisodesChange = (event) => {
    const value = event.target.value;
    setEpisodes(value);
  };

  const handleGenreChange = (event) => {
    const value = event.target.value;
    setGenre(value);
  };

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  };

  const handleStudioChange = (event) => {
    const value = event.target.value;
    setStudio(value);
  };

  // Call useState to create a variable and function to hold list of genres
  const [genreList, setGenreList] = useState([]);
  // Make a request to API to get a list of genres
  const getGenreData = async () => {
    const url = "http://localhost:8000/api/genres/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data.genres);
      setGenreList(data.genres);
    }
  };
  // Call useEffect and send in genGenreData() function to update React elements
  useEffect(() => {
    getGenreData();
  }, []);

  // Form Stuff (do not edit this code)
  const [formData, setFormData] = useState({
    name: "",
    num_seasons: "",
    num_episodes: "",
    description: "",
    genre: "",
    studio: "",
  });

  const [studioList, setStudioList] = useState([]);
  const getStudioData = async () => {
    const url = "http://localhost:8000/api/studios/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data.studios);
      setStudioList(data.studios);
    }
  };
  useEffect(() => {
    getStudioData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.num_seasons = seasons;
    data.num_episodes = episodes;
    data.description = description;
    data.genre = genre;
    data.studio = studio;
    console.log(data);

    const seriesUrl = "http://localhost:8000/api/series/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(seriesUrl, fetchConfig);
    if (response.ok) {
      const newSeries = await response.json();
      console.log(newSeries);

      setName("");
      setSeasons("");
      setEpisodes("");
      setDescription("");
      setGenre("");
      setStudio("");
    }
  };

  return (
    <div>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            Series Name: <b>{name}</b>
            <br /># Seasons: <b>{seasons}</b>
            <br /># Episodes: <b>{episodes}</b>
            <br />
            Description: <b>{description}</b>
            <br />
            Genre: <b>{genre}</b>
            <br />
            Studio: <b>{studio}</b>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new series</h1>
            <form onSubmit={handleSubmit} id="create-series-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleNameChange}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleSeasonsChange}
                  placeholder="Seasons"
                  required
                  type="number"
                  name="seasons"
                  id="seasons"
                  className="form-control"
                />
                <label htmlFor="seasons"># Seasons</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleEpisodesChange}
                  placeholder="Episodes"
                  required
                  type="number"
                  name="episodes"
                  id="episodes"
                  className="form-control"
                />
                <label htmlFor="episodes"># Episodes</label>
              </div>
              <div className="form-floating mb-3">
                <textarea
                  onChange={handleDescriptionChange}
                  placeholder="Description"
                  name="description"
                  id="description"
                  className="form-control"
                />
                <label htmlFor="description">Description</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleGenreChange}
                  required
                  name="genre"
                  id="genre"
                  className="form-select"
                >
                  <option value="">Choose a genre</option>
                  {genreList.map((genre) => {
                    return (
                      <option key={genre.name} value={genre.name}>
                        {genre.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleStudioChange}
                  required
                  name="studio"
                  id="studio"
                  className="form-select"
                >
                  <option value="">Choose a studio</option>
                  {studioList.map((studio) => {
                    return (
                      <option key={studio.name} value={studio.name}>
                        {studio.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SeriesForm;
