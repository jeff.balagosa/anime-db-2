import { useEffect, useState } from 'react';

function SeriesList() {
  const [series, setSeries] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8000/api/series/');

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setSeries(data.series)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th># Seasons</th>
          <th># Episodes</th>
          <th>Genre</th>
          <th>Studio</th>
        </tr>
      </thead>
      <tbody>
        {series.map(serie => {
          return (
            <tr key={serie.href}>
              <td>{ serie.name }</td>
              <td>{ serie.num_seasons }</td>
              <td>{ serie.num_episodes }</td>
              <td>{ serie.genre }</td>
              <td>{ serie.studio }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default SeriesList;
